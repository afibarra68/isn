/**
 * View Models used by Spring MVC REST controllers.
 */
package co.edu.sena.bikestore.web.rest.vm;
