/**
 * Data Access Objects used by WebSocket services.
 */
package co.edu.sena.bikestore.web.websocket.dto;
