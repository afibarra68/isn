import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BikestoreSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [BikestoreSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent]
})
export class BikestoreHomeModule {}
